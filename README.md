# Hogeschool Inholland Parallel Distributed Processing
This Git Repository is for the course Big Data & AI, Parallel Distributed Processing lectures. </ br>

**Each assignment contains multiple assessments**

## Table of contents

* [Assignment 1](#assignment 1)
* [Assignment 2](#assignment 2)
* [Assignment 3](#assignment 3)

## Assignment 1
**The folder for `Assignment 1` includes the code and the results**

* Assessment 1, counting the number of ratings for movies
* Assessment 2, sorting the movies based on rating

## Assignment 2
**The folder for `Assignment 2` includes the code and the results**

* Assessment 1, making a alphabetic list from locations, grouped and counted
* Assessment 2, listing how many times every country won
* Assessment 3, listing the top 10 countries that had the most turns and won the game, no index, fully named

## Assignment 3
**The folder for `Assignment 3` includes the code and the results**

* Question 1, calculate the conditional probability that survives given their sex and passenger-class
* Question 2, what probability has a child that is less or equal than 10 years old survives?
* Question 3, How much did people pay to be on the ship? Calculate the expectation of fare conditioned on passenger-class.