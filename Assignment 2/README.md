# Hogeschool Inholland Parallel Distributed Processing

## Introduction - How to execute
This assignment is separated into multiple assessments. 
The following steps have to be taken in order to execute the scripts for this assignment.

The first steps are downloading the dataset from Moodle. Also an Hadoop node or cluster needs to be running in order to execute the scripts. In our case we have a pre-installed VM.

After these steps are done, the follwing steps have to be taken.

* Startup the Hadoop software/VM
* Make a SSH connection to the machine and sign in 
* Sign in using `maria_dev` as username and as password
* Once in the Ambari interface, navigate to the `Files view`
* In the upper right corner  click `Create new folder`
* Create a folder named `Diplomacy`
* Inside that folder, upload all the CSV files from the dataset downloaded from Moodle
* In the upper right corner choose `Pig view`
* Click the `New script` button
* Use the script that is given in the assessment folder (.txt) file, this contains the script that needs to be run
* Click the `Execute` button in order to execute the Pig script
* Check the result of the script

These steps have to be taken for the other assessments to.

## Assessment 1 - Grade 6
* Make a alphabetic list from all locations from the orders.csv
* Group by “location” with target “Holland”
* Count how many times Holland was the target from that
  location

## Assessment 2 - Grade 7
* Make a list of how many times the country won

## Assessment 3 - Grade 8
* Make a list from the top 10 countries that had the most turns
  and won the game
* Names had to be full name and not the index

## Results
The results of the assessments are shown below.

### Result - Assessment 1
The result of assessment 1
![](Assessment%201/assessment_1_result.PNG)

### Result - Assessment 2
The result of assessment 2
![](Assessment%202/assessment_2_result.PNG)

### Result - Assessment 3
The result of assessment 3
![](Assessment%203/assessment_3_result.PNG)