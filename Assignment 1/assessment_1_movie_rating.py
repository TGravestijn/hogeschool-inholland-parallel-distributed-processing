from mrjob.job import MRJob
from mrjob.step import MRStep

# Class for movie ratings
class RatingsBreakdown (MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_movieIDs,
                   reducer=self.reducer_count_ratings)
        ]

    # Mapper for gettings movieID's
    def mapper_get_movieIDs(self, _, line):
        (userID, movieID, rating, timestamp) = line.split('\t')
        yield movieID, 1

    # Reducer for counting the ratings
    def reducer_count_ratings (self, movieID, number_ratings):
        yield movieID, sum(number_ratings)

if __name__ == '__main__':
    RatingsBreakdown.run()