from mrjob.job import MRJob
from mrjob.step import MRStep

# Class for movie ratings
class RatingsBreakdown (MRJob):

    # Sorting the values for results
    MRJob.SORT_VALUES = True

    # Steps for MRjob
    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_movieIDs,
                   reducer=self.reducer_count_ratings),
            MRStep(mapper=self.mapper_sort_ratings,
                   reducer=self.reducer_sorted_ratings)
        ]

    # Mapper for getting the movieID's
    def mapper_get_movieIDs(self, _, line):
        (userID, movieID, rating, timestamp) = line.split('\t')
        yield int(movieID), 1

    # Reducer for counting the ratings
    def reducer_count_ratings (self, movieID, ratings):
        yield movieID, sum(ratings)

    # Mapper for sorting the ratings
    def mapper_sort_ratings(self, movieID, rating_total):
        yield ('%020d' % int(rating_total), int(movieID))

    # Reducer for sorting the ratings
    def reducer_sorted_ratings(self, rating_total, movieID):
        for id in movieID:
            yield str(id), int(rating_total)

if __name__ == '__main__':
    RatingsBreakdown.run()