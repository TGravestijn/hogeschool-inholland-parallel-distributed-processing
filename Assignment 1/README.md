# Hogeschool Inholland Parallel Distributed Processing

## Table of contents

* [How to run](#how to run)
* [Assessment 1](#assessment 1)
* [Assessment 2](#assessment 2)
* [Results](#results)

## How to run
* Make a SSH connection to the VM (username: `maria_dev` and password: `maria_dev`)
* Become root in the system ( `su`)
* Navigate to the directory to download this repository, for example the home directory (`cd ~`)
* Clone this repository with the following command `git clone https://gitlab.com/TGravestijn/hogeschool-inholland-parallel-distributed-processing.git`
* Navigate into the directory
* Execute the scripts using the following commands 
* `python assessment_1_movie_rating.py u.data`
* `python assessment_2_movie_rating.py u.data`

## Assessment 1
Count the number of ratings given for each movie 
Code have to run on HDFS with a basic setup (Lecture 1 & 2) 
Code is executed with a python command with MrJob

## Assessment 2
Sort the movies by their numbers of ratings

## Results
The results of the assessments

### Assignment 1
The result of assessment 1
![](results/assessment_1_result.png)

### Assignment 2
The result of assessment 2
![](results/assessment_2_result.png)